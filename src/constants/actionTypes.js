export const TEXT_ONLY = 'TEXT_ONLY';
export const IMAGE_ONLY = 'IMAGE_ONLY';
export const TEXT_IMAGE = 'TEXT_IMAGE';
export const NOTHING = 'NOTHING';
export const TOGGLE_LIKE_STATUS = 'TOGGLE_LIKE_STATUS';
export const ADD_COMMENT = 'ADD_COMMENT';
