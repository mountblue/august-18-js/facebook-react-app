import React from 'react';
import Menu from './Menu';
import ItemFeed from '../containers/ItemFeed';
import '../styles/App.css';

const App = () => {
  return (
    <div className="App">
      <Menu />
      <ItemFeed />
    </div>
  );
}
export default App;
