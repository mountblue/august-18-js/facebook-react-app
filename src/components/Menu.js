import React, { Component } from 'react';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getSelectedItems } from '../actions/index';
import { TEXT_ONLY, IMAGE_ONLY, TEXT_IMAGE, NOTHING } from '../constants/actionTypes';
import '../styles/menu.css';
class Menu extends Component {
    constructor(props) {
        super(props);
        this.getSelectedItem = this.getSelectedItem.bind(this);
    }

    getSelectedItem(e) {
        this.props.getSelectedItems(e.target.value);
    }

    render() {
        return (
            <div className="menu">
                <div className="item-size box">
                    <select ref="itemlist" className="dropdown" onChange={this.getSelectedItem}>
                        <option className="item" value={TEXT_ONLY}>Text Only</option>
                        <option className="item" value={IMAGE_ONLY}>Images Only</option>
                        <option className="item" value={TEXT_IMAGE}>Image and Text</option>
                        <option className="item" value={NOTHING}>No items</option>
                    </select>
                </div>
            </div>

        );
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getSelectedItems: getSelectedItems//The action creators in actions folder. when dispatched, all the reducers will be notified, and action type will be handled under appropriate cases.

    }, dispatch);
}


export default connect(null, matchDispatchToProps)(Menu);

