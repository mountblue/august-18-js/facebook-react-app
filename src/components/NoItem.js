import React from 'react';

const NoItem = () => {
    return (
        <div>
            <h1>No items to show</h1>
        </div>
    );

}

export default NoItem;
