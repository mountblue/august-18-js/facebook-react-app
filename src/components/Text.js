import React from 'react';
import '../styles/text.css';

const Text = (props) => {
    return (
        <div className="statusText">
            <p>{props.text}</p>
        </div>
    );
}

export default Text;