import React from 'react';
import Comments from './Comments';
import Text from './Text';
import Image from './Image';
import Like from '../containers/Like';
import Comment from '../containers/Comment';
import '../styles/item.css';

const Item = (props) => {
    return (
        <div className="itemDiv">
            {props.item.item_description.length ? <Text text={props.item.item_description} /> : null}
            {props.item.image.length ? <Image /> : null}
            <Like item={props.item} />
            <Comment item={props.item} />
            <Comments comments={props.item.comments} />
        </div>
    );
}

export default Item;

