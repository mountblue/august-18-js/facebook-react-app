import React from 'react';
import pic from '../images/drew.jpeg';

const Image = () => {
    return (
        <div>
            <img src={pic} alt="pic" />
        </div>
    );
}

export default Image;