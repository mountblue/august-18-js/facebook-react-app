import React from 'react';
import '../styles/comments.css';

const Comments = (props) => {

    let commentList = props.comments.map(item => {
        return (
            <div className="commentDiv">
                <p key={item.id} className="commentText">{item.comment}</p>
            </div>
        );
    });
    return (
        <div>
            {commentList}
        </div>
    );

}

export default Comments;
