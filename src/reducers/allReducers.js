import { combineReducers } from 'redux';
import itemsList from './reducerItems';

const allReducers = combineReducers({
    itemsList: itemsList
});

export default allReducers;
