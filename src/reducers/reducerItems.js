import data from '../services/Data';
import { TEXT_ONLY, IMAGE_ONLY, TEXT_IMAGE, NOTHING, TOGGLE_LIKE_STATUS, ADD_COMMENT } from '../constants/actionTypes';

let initialState = {
    items: data.getItems().filter(item => item.item_description.length && !item.image.length)
};

export default function (state = initialState, action) {
    switch (action.type) {
        case TEXT_ONLY:
            console.log('reducer text only');
            let newState = { ...state };

            newState.items = data.getItems().filter(item => item.item_description.length && !item.image.length);
            return newState;
        case IMAGE_ONLY:
            console.log('reducer image only');
            newState = { ...state };

            newState.items = data.getItems().filter(item => !item.item_description.length && item.image.length);
            return newState;
        case TEXT_IMAGE:
            console.log('reducer text image only');
            newState = { ...state };

            newState.items = data.getItems().filter(item => item.item_description.length && item.image.length);
            return newState;
        case NOTHING:
            newState = { ...state };

            newState.items = [];
            return newState;
        case TOGGLE_LIKE_STATUS:
            let newItems = [];
            state.items.forEach((item) => {
                let newItem = { ...item };
                if (item.id === action.itemId) {
                    newItem.isliked ? newItem.likes-- : newItem.likes++;
                    newItem.isliked = !newItem.isliked;
                }
                newItems.push(newItem);
            });
            return { ...state, items: newItems };
        case ADD_COMMENT:
            newItems = [];
            state.items.forEach((item) => {
                let newItem = { ...item };
                if (item.id === action.itemId) {
                    let newComments = [...item.comments];
                    let newComment = new data.getNewComment();
                    newComment.id = newComments.length + 1;
                    newComment.comment = action.commentText;
                    newComments.push(newComment);
                    newItem = { ...newItem, comments: newComments }
                }
                newItems.push(newItem);
            });
            return { ...state, items: newItems };

        default: return state;
    }
}