export const addComment = (itemId, commentText) => {
    console.log('comment added');
    return ({
        type: "ADD_COMMENT",
        itemId: itemId,
        commentText: commentText
    });
}

export const getSelectedItems = (type) => {
    return ({
        type: type
    });
}

export const toggleLikeStatus = (itemId) => {
    return ({
        type: "TOGGLE_LIKE_STATUS",
        itemId: itemId
    });
}


