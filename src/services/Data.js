import { Component } from 'react';
import data from './Data.json';

export default class Data extends Component {
    static getItems() {
        return [...data];
    }

    static getNewComment() {
        this.id = 0;
        this.comment = '';
        this.created_at = '';
    }
}