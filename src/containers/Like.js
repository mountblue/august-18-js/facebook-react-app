import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toggleLikeStatus } from '../actions/index';
import '../styles/like.css';

class Like extends Component {
    constructor(props) {
        super(props);
        this.toggleLikeStatus = this.toggleLikeStatus.bind(this);
    }
    toggleLikeStatus() {
        this.props.toggleLikeStatus(this.props.item.id);
    }
    render() {
        return (
            <div className>
                <div className="likeButtonDiv">
                    <button className="comments" onClick={this.toggleLikeStatus}>Like</button>
                    <div className="likeCount"><p>{this.props.item.likes}</p></div>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        itemsList: state.itemsList
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        toggleLikeStatus: toggleLikeStatus

    }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(Like);