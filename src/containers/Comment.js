import React, { Component } from 'react';
import { addComment } from '../actions/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../styles/comments.css';

class Comment extends Component {
    constructor(props) {
        super(props);
        this.getCommentText = this.getCommentText.bind(this);
        this.addComment = this.addComment.bind(this);
        this.state = {
            commentText: ""
        }
    }

    addComment() {
        this.props.addComment(this.props.item.id, this.state.commentText);
        this.setState({ commentText: '' });
    }

    getCommentText(e) {
        this.setState({ commentText: e.target.value });
    }

    render() {
        return (
            <div>
                <div className="buttonDiv"><button className="comments">Comments</button></div>
                <div className="commentInputDiv">
                    <input type="text" placeholder="Add a comment" onChange={this.getCommentText} value={this.state.commentText} />
                    <button type="submit" onClick={this.addComment}>Add</button>
                </div>
            </div>
        );
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        addComment: addComment
    }, dispatch);
}


export default connect(null, matchDispatchToProps)(Comment);

