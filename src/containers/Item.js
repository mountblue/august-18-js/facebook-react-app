import React from 'react';
import Comments from '../components/Comments';
import Text from '../components/Text';
import Image from '../components/Image';
import Like from './Like';
import Comment from './Comment';
import '../styles/item.css';

const Item = (props) => {
    return (
        <div className="itemDiv">
            {props.item.item_description.length ? <Text text={props.item.item_description} /> : null}
            {props.item.image.length ? <Image /> : null}
            <Like item={props.item} />
            <Comment item={props.item} />
            <Comments comments={props.item.comments} />
        </div>
    );
}

export default Item;

