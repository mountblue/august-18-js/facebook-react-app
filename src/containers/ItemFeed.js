import React, { Component } from 'react';
import { connect } from 'react-redux';
import Item from '../components/Item';
import NoItem from '../components/NoItem';
import '../styles/item-feed.css';

class ItemFeed extends Component {
    render() {
        if (!this.props.itemsList.items.length) return <NoItem />
        let itemsList = this.props.itemsList.items.map(item => {
            return (
                <Item
                    key={item.id}
                    item={item}
                />
            );
        });

        return (
            <div className="item-feed">
                {itemsList}
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        itemsList: state.itemsList
    };
}

export default connect(mapStateToProps)(ItemFeed);